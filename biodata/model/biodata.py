from odoo import api,fields,models

class PickingList(models.Model):
    _name = "entry.biodata"
    _inherit = ['mail.thread', 'mail.activity.mixin']

    name = fields.Char(
        'Reference', default='/',
        copy=False, index=True,
        states={'done': [('readonly', True)], 'cancel': [('readonly', True)]})
    posisi = fields.Char('Posisi yang dilamar')
    nama = fields.Char('Nama')
    no_ktp = fields.Char('No KTP')
    ttl = fields.Char('Tempat, Tanggal Lahir')
    date = fields.Datetime(
        'Creation Date',
        default=fields.Datetime.now, index=True, track_visibility='onchange',
        states={'done': [('readonly', True)], 'cancel': [('readonly', True)]},
        help="Creation Date, usually the time of the order")
    note = fields.Text('Notes')
    move_line_ids = fields.One2many('biodata.line', 'bio_id', 'Operations')
    riwpel_line_ids = fields.One2many('riwayat.line', 'riwpel_id', 'Riwayat Pel')
    riwpek_line_ids = fields.One2many('riwayatpek.line', 'riwpek_id', 'Riwayat Pek')
    state = fields.Selection([
        ('draft', 'Open'),
        ('done', 'Done'),
        ('cancel', 'Cancelled'),
    ], string='Status', default='draft')
    agama = fields.Selection([('islam', 'Islam'),
                              ('kristen','Kristen'),
                              ('katolik', 'Katolik'),
                              ('hindu', 'Hindu'),
                              ('budha', 'Budha')], string="Agama")
    jenis_kelamin = fields.Selection([('laki', 'Laki-laki'),
                                      ('perempuan', 'Perempuan')], string="Jenis Kelamin", default="laki")
    image = fields.Binary(string="Image")
    gol_darah = fields.Char('Golongan Darah')
    status = fields.Selection([('single', 'Single'),
                                      ('menikah', 'Sudah Menikah')], string="Status")
    alamat_ktp = fields.Text('Alamat KTP')
    alamat_dom = fields.Text('Alamat Tinggal')
    email = fields.Char('Email')
    telp = fields.Char('No Telp')

    @api.multi
    def action_cancel(self):
        self.write({'state': 'canceled'})
        return True

    @api.multi
    def action_draft(self):
        self.write({'state': 'draft'})
        return True

class PickingListLine(models.Model):
    _name = "biodata.line"
    _description = "Last Edu Line"

    jenjang = fields.Selection([('sma', 'SMA'),
                              ('s1','S1'),
                              ('s2', 'S2')], string="Pendidikan Terakhir")
    institusi = fields.Char('Nama Institusu Akademik')
    jurusan = fields.Char('Jurusan')
    tahun_lul = fields.Integer('Tahun Lulus')
    ipk = fields.Float('IPK')
    bio_id = fields.Many2one('entry.biodata', "Bio", ondelete='cascade', index=True)

class RiwayatPelatihan(models.Model):
    _name = "riwayat.line"
    _description = "Riwayat Pelatihan"

    kursus = fields.Char('Nama Kursus Seminar')
    sertifikat = fields.Selection([('ada', 'ADA'),
                                      ('tidak', 'Tidak')], string="Sertifikat (Ada/Tidak)")
    tahun_ber = fields.Char('Tahun')
    riwpel_id = fields.Many2one('entry.biodata', "Riwayat Pel")

class RiwayatPekerjaan(models.Model):
    _name = "riwayatpek.line"
    _description = "Riwayat Pekerjaan"

    nama_comp = fields.Char('Nama Perusahaan')
    posisi_terakhir = fields.Char('Posisi Terakhir')
    pendapatan_terakhir = fields.Integer('Pendapatan Terakhir')
    tahun_ker = fields.Char('Tahun Kerja')
    riwpek_id = fields.Many2one('entry.biodata', "Riwayat Pek")

