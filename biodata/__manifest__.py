{
    'name': 'Entry Biodata',
    'version': '1.0',
    'category': 'Inventory',
    'website': '',
    'author' : 'Fany Achmad',
    'depends': ['mail','base'],
    'data' : [
            'views/biodata.xml',
        ],
    'installable': 'True',
}